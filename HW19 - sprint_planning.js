const allWorkersCapacity = [{ 'Ban': 25 }, { 'Jack': 33 }, { 'Sarah': 45 }, { 'Max': 87 }, { 'Molly': 11 }, { 'Leo': 18 }];
const projectsBacklog = [1200, 500, 500, 1000, 300];

const workersSpeed = allWorkersCapacity.map(obj => Object.values(obj)).flat(); // [25, 33, 45, 87, 11, 18]
const deadLine = new Date('28 Jan 2023');


function sprintPlanning(workersSpeed, backlog, deadLine) {
   const groupCapacityPerDay = workersSpeed.reduce((a, b) => a + b); //производительность всей группы за 1 день
   const totalProjectPoints = backlog.reduce((a, b) => a + b); //общая сложность всех заданий

   const presentDay = Date.now(); // сегодняшняя дата на момент расчета
   const oneDay = 1000 * 60 * 60 * 24; // кол-во миллисикунд в сутках

   let day = new Date(presentDay).getDay(); // номер дня недели (стартует с сегодняшнего дня)
   let workDays = 0;
   let _nextDay = presentDay;

   const availableDays = Math.ceil(totalProjectPoints / groupCapacityPerDay); // необходимое кол-во рабочих дней
   let daysToDeadline = Math.ceil((Date.parse(deadLine) - presentDay) / oneDay); // оставшееся кол-во дней до дедлайна
   
   console.log('Необхідна кількість робочих днів', availableDays);
   
   while (_nextDay < deadLine) {
      if (day < 7) {
         if (day !== 6 && day !== 0) {
            daysToDeadline--;
            workDays++;
            if (workDays === availableDays) return console.log(`Усі завдання будуть успішно виконані за ${daysToDeadline} днів до настання дедлайну!`);
         } else daysToDeadline--;
         _nextDay += oneDay;
         day++
      } else {
         day = 0;
      }
   }
   return console.log(`Команді розробників доведеться витратити додатково ${(availableDays - workDays) * 8} годин або ${availableDays - workDays} робочих днів після дедлайну, щоб виконати всі завдання в беклозі`);
}

sprintPlanning(workersSpeed, projectsBacklog, deadLine);